<?php session_start(); ?>

<!DOCTYPE html>

<html class=" js flexbox canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers no-applicationcache svg inlinesvg smil svgclippaths"><!--<![endif]--><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
   
   
    <title>Signature Club Membership Destinations | Travel Beans</title>
    
    <!-- Meta Tags -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="author" content="Travel Beans">
    <meta name="description" content="">
    <meta name="distribution" content="global">
    <meta name="country" content="india">
    <meta name="copyright" content="Travel Beans">
    <meta name="revisit-after" content="1 hour">
    <meta name="designer" content="Travel Beans">
    <meta name="language" content="english">
    <meta name="robots" content="index, follow">
    <meta name="rating" content="general">
    <meta name="publisher" content="Travel Beans">

    <meta property="og:title" content="Travel Beans">
    <meta property="og:description" content="">
    <meta property="og:image" content="">
  
    <meta name="twitter:card" content="">
  
    <meta name="keywords" content="">
    <meta name="format-detection" content="telephone=no">
    
	
	
	<link rel="shortcut icon" href="" type="image/x-icon">
    
    <!-- Theme Styles -->
    <link rel="stylesheet" href="./Membership-Destinations_files/bootstrap.min.css">
    <link rel="stylesheet" href="./Membership-Destinations_files/font-awesome.min.css">
    <link href="./Membership-Destinations_files/css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="./Membership-Destinations_files/animate.min.css">
    
    <!-- Main Style -->
    <!--<link id="main-style" rel="stylesheet" href="css/style.css">-->
    <script type="text/javascript">
        var mainStyle = "style.css";
        if (typeof localStorage != "undefined") {
            var colorSkin = localStorage.getItem("colorSkin");
            if (colorSkin != null) {
                mainStyle = "style-" + colorSkin + ".css";
            }
        }
        document.write('<link id="main-style" rel="stylesheet" href="css/' + mainStyle + '">');
    </script><link id="main-style" rel="stylesheet" href="./Membership-Destinations_files/style-orange.css">
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <!-- Updated Styles -->
    <link rel="stylesheet" href="./Membership-Destinations_files/updates.css">

    <!-- Custom Styles -->
    <link rel="stylesheet" href="./Membership-Destinations_files/custom.css">
    
    <!-- Responsive Styles -->
    <link rel="stylesheet" href="./Membership-Destinations_files/responsive.css">
    
    
</head>
<body>
    <div id="page-wrapper">
        
    <?php include('php_session.php'); ?>
        
		
                    <header id="header" class="navbar-static-top">
                        <div class="topnav hidden-xs">
                            <div class="container">
                                <!-- <ul class="quick-menu pull-left">
                                    <li><a href="http://www.thetravelsqaure.com">THE TRAVEL SQAURE</a></li>
                                    <li><a href="http://www.travelbeans.in">TRAVEL BEANS</a></li>
                                </ul> -->
                                <ul class="quick-menu pull-right">
                                <li><a href="login.php">Login</a></li>
                                    <li class="ribbon">
                                        <a href=""><?php if($_SESSION["username"] == true){ echo "Hi! "." ".htmlspecialchars($row['name']); } ?></a><i class="fa fa-angle-down"></i>
                                        <ul class="menu mini uppercase left">
                                            <li><a href="partnerdashboard.php" class="location-reload">Dashboard</a></li>
                                            <li><a href="logout.php">Signout</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
            
            <div class="main-header">
                
                <a href="#mobile-menu-01" data-toggle="collapse" class="mobile-menu-toggle">
                    Mobile Menu Toggle
                </a>

                <div class="container">
                    <h1 class="navbar-brand" style="height: 20%;">
                        <a href="http://www.thetravelsqaure.com" title="Signature Club">
                            <img src="logo.jpg" alt="Signature Club by The Travel Square">
                        </a>
                    </h1>
                    
                    <nav id="main-menu" role="navigation">
                        <ul class="menu">
                            <li class="menu-item-has-children">
                                <a href="index.php">Home</a>
                                <ul>
                                </ul>
                            <li class="menu-item-has-children active">
                                    <a href="about.php">About Signature Club</a>
                                </li>
                                <li class="menu-item-has-children">
                                    <a href="destinations.php">Worldwide Destinations</a>
                                </li>
                                <li class="menu-item-has-children">
                                        <a href="#">Membership Plans</a>
                                        <ul>
                                            <li><a href="premium.php">Premium Membership</a></li>
                                            <li><a href="prestige.php">Prestige Membership</a></li>
                                            <li><a href="privilege.php">Privilege Membership</a></li>
                                        </ul>
                                    </li>
                                <li class="menu-item-has-children">
                                    <a href="thetravelsquare.com">Contact Us</a>
                                </li>
                           
                           
                            
                            
                        </ul>
                    </nav>
                </div>
                
                <nav id="mobile-menu-01" class="mobile-menu collapse">
                    <ul id="mobile-primary-menu" class="menu">
                        <li class="menu-item-has-children">
                            <a href="index.php">Home</a><button class="dropdown-toggle collapsed" data-toggle="collapse" data-target="#mobile-menu-submenu-item-0"></button>
                            
                        </li>
                        <li class="menu-item-has-children">
                            <a href="">Hotels</a><button class="dropdown-toggle collapsed" data-toggle="collapse" data-target="#mobile-menu-submenu-item-2"></button>
                        </li>   
                    </ul>
                    
                    <ul class="mobile-topnav container">
                        <li><a href="partnerdashboard.php">MY ACCOUNT</a></li>
                        <li><a href="login.php" class="soap-popupbox">LOGIN</a></li>
                    </ul>   
                </nav>
				
				
            </div>
        </header>

        <section id="content" class="no-padding">
            <div class="banner imagebg-container" style="height: 350px; background-image: url(images/Travel-Beans-Membership-Destinations.jpg);">
                
            </div>
            
            <div class="container">
                <div id="main" class="md-section">
				
					<h2 class="box">World's Best Curated Holiday Destinations , More Than 250+ Hotels at 100+ Destinations in the World</h2></br>
					
					
					<center><h1 class="box">Domestic</h1></center>
                    <div class="more-popular-destinations">
                        <div class="row">
                            <div class="col-sm-6 col-md-3">
                                <ul class="triangle hover">
                                    <li><a href="#"><h5 class="box-title">Delhi<small>7 Properties</small></h5></a></li>
                                    <li><a href="#"><h5 class="box-title">Ghaziabad<small>2 Properties</small></h5></a></li>
                                    <li><a href="#"><h5 class="box-title">Noida<small>1 Properties</small></h5></a></li>
                                    <li><a href="#"><h5 class="box-title">Gurgaon<small>8 Properties</small></h5></a></li>
                                    <li><a href="#"><h5 class="box-title">Agra<small>3 Properties</small></h5></a></li>
                                </ul>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <ul class="triangle hover">
                                    <li><a href="#"><h5 class="box-title">Manali<small>6 Properties</small></h5></a></li>
                                    <li><a href="#"><h5 class="box-title">Amritsar<small>4 Properties</small></h5></a></li>
                                    <li><a href="#"><h5 class="box-title">Bangalore<small>6 Properties</small></h5></a></li>
                                    <li><a href="#"><h5 class="box-title">Barmer<small>1 Properties</small></h5></a></li>
                                    <li><a href="#"><h5 class="box-title">Bhubaneshwar<small>1 Properties</small></h5></a></li>
                                </ul>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <ul class="triangle hover">
                                    <li><a href="#"><h5 class="box-title">Chandigarh<small>1 Properties</small></h5></a></li>
                                    <li><a href="#"><h5 class="box-title">Chennai<small>5 Properties</small></h5></a></li>
                                    <li><a href="#"><h5 class="box-title">Chittorgarh<small>1 Properties</small></h5></a></li>
                                    <li><a href="#"><h5 class="box-title">Cochin<small>3 Properties</small></h5></a></li>
                                    <li><a href="#"><h5 class="box-title">Darjeeling<small>3 Properties</small></h5></a></li>
                                </ul>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <ul class="triangle hover">
                                    <li><a href="#"><h5 class="box-title">Dehradun<small>1 Properties</small></h5></a></li>
                                    <li><a href="#"><h5 class="box-title">Dharamshala<small>2 Properties</small></h5></a></li>
                                    <li><a href="#"><h5 class="box-title">Gantok<small>2 Properties</small></h5></a></li>
                                    <li><a href="#"><h5 class="box-title">Goa<small>9 Properties</small></h5></a></li>
                                    <li><a href="#"><h5 class="box-title">Haridwar<small>1 Properties</small></h5></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
					<div class="more-popular-destinations">
                        <div class="row">
                            <div class="col-sm-6 col-md-3">
                                <ul class="triangle hover">
                                    <li><a href="#"><h5 class="box-title">Hyderabad<small>3 Properties</small></h5></a></li>
                                    <li><a href="#"><h5 class="box-title">Indore<small>1 Properties</small></h5></a></li>
                                    <li><a href="#"><h5 class="box-title">Jaipur<small>5 Properties</small></h5></a></li>
                                    <li><a href="#"><h5 class="box-title">Jaisalmer<small>1 Properties</small></h5></a></li>
                                    <li><a href="#"><h5 class="box-title">Jodhpur<small>2 Properties</small></h5></a></li>
                                </ul>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <ul class="triangle hover">
                                    <li><a href="#"><h5 class="box-title">Katra<small>1 Properties</small></h5></a></li>
                                    <li><a href="#"><h5 class="box-title">Kerela<small>3 Properties</small></h5></a></li>
                                    <li><a href="#"><h5 class="box-title">Kumbalgarh<small>2 Properties</small></h5></a></li>
                                    <li><a href="#"><h5 class="box-title">Munnar<small>1 Properties</small></h5></a></li>
                                    <li><a href="#"><h5 class="box-title">Mahabaleshwar<small>1 Properties</small></h5></a></li>
                                </ul>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <ul class="triangle hover">
                                    <li><a href="#"><h5 class="box-title">Mumbai<small>8 Properties</small></h5></a></li>
                                    <li><a href="#"><h5 class="box-title">Mussorie<small>2 Properties</small></h5></a></li>
                                    <li><a href="#"><h5 class="box-title">Mysore<small>3 Properties</small></h5></a></li>
                                    <li><a href="#"><h5 class="box-title">Nainital<small>2 Properties</small></h5></a></li>
                                    <li><a href="#"><h5 class="box-title">Ooty<small>2 Properties</small></h5></a></li>
                                </ul>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <ul class="triangle hover">
                                    <li><a href="#"><h5 class="box-title">Ahemdabad<small>3 Properties</small></h5></a></li>
                                    <li><a href="#"><h5 class="box-title">Alleppey<small>3 Properties</small></h5></a></li>
                                    <li><a href="#"><h5 class="box-title">Aurangabad<small>1 Properties</small></h5></a></li>
									<li><a href="#"><h5 class="box-title">Jim Corbet<small>2 Properties</small></h5></a></li>
                                    <li><a href="#"><h5 class="box-title">Shirdi<small>1 Properties</small></h5></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
					<div class="more-popular-destinations">
                        <div class="row">
                            <div class="col-sm-6 col-md-3">
                                <ul class="triangle hover">
                                    <li><a href="#"><h5 class="box-title">Puri<small>1 Properties</small></h5></a></li>
                                    <li><a href="#"><h5 class="box-title">Pushkar<small>1 Properties</small></h5></a></li>
                                    <li><a href="#"><h5 class="box-title">Ranthambore<small>1 Properties</small></h5></a></li>
                                    
                                </ul>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                <ul class="triangle hover">
                                    <li><a href="#"><h5 class="box-title">Thirvanthapuram<small>1 Properties</small></h5></a></li>
                                    <li><a href="#"><h5 class="box-title">Vrindavan<small>3 Properties</small></h5></a></li>
                                    <li><a href="#"><h5 class="box-title">Thekkady<small>2 Properties</small></h5></a></li>
                                    
                                </ul>
                            </div>
                            <div class="col-sm-6 col-md-3">
                                    <ul class="triangle hover">
                                            <li><a href="#"><h5 class="box-title">Shimla<small>1 Properties</small></h5></a></li>
                                            <li><a href="#"><h5 class="box-title">Udaipur<small>2 Properties</small></h5></a></li>
                                        
                                    </ul>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                        <ul class="triangle hover">
                                                <li><a href="#"><h5 class="box-title">Vadodra<small>1 Properties</small></h5></a></li>
                                                <li><a href="#"><h5 class="box-title">Ludhiana<small>1 Properties</small></h5></a></li>
                                        </ul>
                                    </div>
                            
                        </div>
                    </div>
					
					<br><br>
				
                    <h2 class="no-margin">Dubai, United Arab Emirates</h2>
                    <div class="small-box"><small class="uppercase fourty-space">3390 Holiday Properties</small></div>
                    <div class="row image-box style1 add-clearfix">
                        <div class="col-sm-6 col-md-3">
                            <article class="box">
                                <figure>
                                    <a class="hover-effect" title="" href="#"><img width="270" height="160" alt="" src="./Membership-Destinations_files/destinations01.jpg"></a>
                                </figure>
                                <div class="details">
                                    <span class="price"><small>FROM</small>$490</span>
                                    <h4 class="box-title">Atlantis - The Palm<small>Paris</small></h4>
                                </div>
                            </article>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <article class="box">
                                <figure>
                                    <a class="hover-effect" title="" href="#"><img width="270" height="160" alt="" src="./Membership-Destinations_files/destinations02.jpg"></a>
                                </figure>
                                <div class="details">
                                    <span class="price"><small>FROM</small>$170</span>
                                    <h4 class="box-title">Hilton Hotel<small>LONDON</small></h4>
                                </div>
                            </article>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <article class="box">
                                <figure>
                                    <a class="hover-effect" title="" href="#"><img width="270" height="160" alt="" src="./Membership-Destinations_files/destinations09.jpg"></a>
                                </figure>
                                <div class="details">
                                    <span class="price"><small>FROM</small>$130</span>
                                    <h4 class="box-title">Beach Rest Place<small>dubai</small></h4>
                                </div>
                            </article>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <article class="box">
                                <figure>
                                    <a class="hover-effect" title="" href="#"><img width="270" height="160" alt="" src="./Membership-Destinations_files/destinations04.jpg"></a>
                                </figure>
                                <div class="details">
                                    <span class="price"><small>FROM</small>$290</span>
                                    <h4 class="box-title">Crown Casino<small>ASUTRALIA</small></h4>
                                </div>
                            </article>
                        </div>
                    </div>

                    <h2 class="no-margin">Kuala Lumpur, Malaysia</h2>
                    <div class="small-box"><small class="uppercase fourty-space">2812 Holiday Properties</small></div>
                    <div class="row image-box style1 add-clearfix">
                        <div class="col-sm-6 col-md-3">
                            <article class="box">
                                <figure>
                                    <a class="hover-effect" title="" href="#"><img width="270" height="160" alt="" src="./Membership-Destinations_files/4(1).png"></a>
                                </figure>
                                <div class="details">
                                    <span class="price"><small>FROM</small>$809</span>
                                    <h4 class="box-title">Girl On Beach<small>Malaysia</small></h4>
                                </div>
                            </article>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <article class="box">
                                <figure>
                                    <a class="hover-effect" title="" href="#"><img width="270" height="160" alt="" src="./Membership-Destinations_files/5(1).png"></a>
                                </figure>
                                <div class="details">
                                    <span class="price"><small>FROM</small>$125</span>
                                    <h4 class="box-title">Beautiful Hotel<small>Malaysia</small></h4>
                                </div>
                            </article>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <article class="box">
                                <figure>
                                    <a class="hover-effect" title="" href="#"><img width="270" height="160" alt="" src="./Membership-Destinations_files/15.png"></a>
                                </figure>
                                <div class="details">
                                    <span class="price"><small>FROM</small>$250</span>
                                    <h4 class="box-title">Dinner on Beach<small>Malaysia</small></h4>
                                </div>
                            </article>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <article class="box">
                                <figure>
                                    <a class="hover-effect" title="" href="#"><img width="270" height="160" alt="" src="./Membership-Destinations_files/11.png"></a>
                                </figure>
                                <div class="details">
                                    <span class="price"><small>FROM</small>$920</span>
                                    <h4 class="box-title">Beach Area<small>Malaysia</small></h4>
                                </div>
                            </article>
                        </div>
                    </div>

                    <h2 class="no-margin">Cape Town, South Africa</h2>
                    <div class="small-box"><small class="uppercase fourty-space">4706 Holiday Properties</small></div>
                    <div class="row image-box style1 add-clearfix">
                        <div class="col-sm-6 col-md-3">
                            <article class="box">
                                <figure>
                                    <a class="hover-effect" title="" href="#"><img width="270" height="160" alt="" src="./Membership-Destinations_files/10.png"></a>
                                </figure>
                                <div class="details">
                                    <span class="price"><small>FROM</small>$312</span>
                                    <h4 class="box-title">Boat Sea Horizon<small>South Africa</small></h4>
                                </div>
                            </article>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <article class="box">
                                <figure>
                                    <a class="hover-effect" title="" href="#"><img width="270" height="160" alt="" src="./Membership-Destinations_files/13.png"></a>
                                </figure>
                                <div class="details">
                                    <span class="price"><small>FROM</small>$299</span>
                                    <h4 class="box-title">Beauty of Venice<small>South Africa</small></h4>
                                </div>
                            </article>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <article class="box">
                                <figure>
                                    <a class="hover-effect" title="" href="#"><img width="270" height="160" alt="" src="./Membership-Destinations_files/3(1).png"></a>
                                </figure>
                                <div class="details">
                                    <span class="price"><small>FROM</small>$421</span>
                                    <h4 class="box-title">MGM Grand<small>South Africa</small></h4>
                                </div>
                            </article>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <article class="box">
                                <figure>
                                    <a class="hover-effect" title="" href="#"><img width="270" height="160" alt="" src="./Membership-Destinations_files/14.png"></a>
                                </figure>
                                <div class="details">
                                    <span class="price"><small>FROM</small>$124</span>
                                    <h4 class="box-title">Near Beach<small>South Africa</small></h4>
                                </div>
                            </article>
                        </div>
                    </div>

                    

                </div>
            </div>
        </section>
        
        <footer id="footer" class="style5">
            <div class="footer-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <h2>The Travel Square</h2>
                            <ul class="discover triangle hover row">
                                <li class="col-xs-6"><a href="http://www.thetravelsquare.in/about-Travel-Square.html">About Company</a></li>
                                <li class="col-xs-6"><a href="http://www.thetravelsquare.in/jobs.html">Jobs & Carreers</a></li>
								<li class="col-xs-6"><a href="http://www.thetravelsquare.in/about-Travel-Square.html">Corporate Offices</a></li>
                                <li class="col-xs-6"><a href="http://www.thetravelsquare.in/legal.html">Legal Information</a></li>
                                <li class="col-xs-6"><a href="http://www.thetravelsquare.in/press.html">Press Release</a></li>
                                <li class="col-xs-6"><a href="http://www.thetravelsquare.in/legal.html">T & C</a></li>
                                <li class="col-xs-6"><a href="http://www.thetravelsquare.in/customer-care.html">Customer Care</a></li>
                                <li class="col-xs-6"><a href="http://www.thetravelsquare.in/legal.html">Privacy Policies</a></li>
                                <li class="col-xs-6"><a href="http://www.thetravelsquare.in/partner-Care.html">Partner Help</a></li>
                                <li class="col-xs-6"><a href="http://www.thetravelsquare.in/faq.html">FAQ's</a></li>
                                <li class="col-xs-6"><a href="http://www.thetravelsquare.in/site-Map.html">Site Map</a></li>
                                <li class="col-xs-6"><a href="http://www.thetravelsquare.in/contact-Us.html">Contact Us</a></li>
                            </ul>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <h2>Our Products</h2>
                            <ul class="discover triangle hover row">
                                <li class="col-xs-"><a href="http://www.thetravelsquare.in/pro">Travel Square Business</a></li>
                                <li class="col-xs-"><a href="http://www.signatureclubmembership.com/">Signature Club</a></li>
                                <li class="col-xs-"><a href="http://www.cruisader.com/">Cruisader</a></li>
                                <li class="col-xs-"><a href="http://www.cruisingindia.in/">Cruising India</a></li>
                                <li class="col-xs-"><a href="http://www.happenings.ind.in/">Happenings</a></li>
                                <li class="col-xs-"><a href="http://www.shoppist.me/">Shoppist</a></li>
                                
                            </ul>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <img src="images/Signature-Club-Logo.jpg">
                            <br>
                            
                            <br>
                            
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <h2>About Us</h2>
                            <p>Signature Club Founded in 2018 , Aims to Provide Complete Luxury During the Journey of Our Members . The Club Offers More than 75+ Destinations With 200+ Hotels , 
	Worldwide Covering Most of the Travel Destinations in the World .</p>
                            <br>
                            <address class="contact-details">
                                <center><a href="" class="contact-email">signatureclubmembership@gmail.com</a></center>
                            </address>
                            <ul class="social-icons clearfix">
                                <li class="twitter"><a title="Join Us On Twitter" href="https://twitter.com/signaturemember" data-toggle="tooltip" data-original-title="twitter" target="blank_"><i class="soap-icon-twitter"></i></a></li>
                                <li class="facebook"><a title="Join Us On Facebook" href="https://www.facebook.com/signatureclubmembership/" target="blank_" data-toggle="tooltip" data-original-title="facebook"><i class="soap-icon-facebook"></i></a></li>
                                <li class="instagram"><a title="Join Us On Instagram" href="https://www.instagram.com/signatureclubmembership/" target="blank_" data-toggle="tooltip" data-original-title="facebook"><i class="soap-icon-instagram"></i></a></li>
                                <li class="linkedin"><a title="Join Us On Linked In" href="" target="blank_" data-toggle="tooltip" data-original-title="linkedin"><i class="soap-icon-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottom gray-area">
                <div class="container">
                    
                    <div class="pull-right">
                        <a id="back-to-top" href="" class="animated bounce" data-animation-type="bounce" style="animation-duration: 1s; visibility: visible;"><i class="soap-icon-longarrow-up circle"></i></a>
                    </div>
                    <div class="copyright pull-right">
                        <p>© 2019 Signature Club | The Travel Square</p>
                    </div>
                </div>
            </div>
        </footer>
    </div>


    <!-- Javascript -->
    <script type="text/javascript" src="./Hotel Collections_files/jquery-1.11.1.min.js.download"></script>
    <script type="text/javascript" src="./Hotel Collections_files/jquery.noconflict.js.download"></script>
    <script type="text/javascript" src="./Hotel Collections_files/modernizr.2.7.1.min.js.download"></script>
    <script type="text/javascript" src="./Hotel Collections_files/jquery-migrate-1.2.1.min.js.download"></script>
    <script type="text/javascript" src="./Hotel Collections_files/jquery.placeholder.js.download"></script>
    <script type="text/javascript" src="./Hotel Collections_files/jquery-ui.1.10.4.min.js.download"></script>
    
    <!-- Twitter Bootstrap -->
    <script type="text/javascript" src="./Hotel Collections_files/bootstrap.js.download"></script>

    <!-- Flex Slider -->
    <script type="text/javascript" src="./Hotel Collections_files/jquery.flexslider-min.js.download"></script>
    
    <!-- parallax -->
    <script type="text/javascript" src="./Hotel Collections_files/jquery.stellar.min.js.download"></script>
    
    <!-- waypoint -->
    <script type="text/javascript" src="./Hotel Collections_files/waypoints.min.js.download"></script>

    <!-- load page Javascript -->
    <script type="text/javascript" src="./Hotel Collections_files/theme-scripts.js.download"></script>
    <script type="text/javascript" src="./Hotel Collections_files/scripts.js.download"></script>

    <script type="text/javascript">
        tjq("#slideshow .flexslider").flexslider({
            animation: "fade",
            controlNav: false,
            animationLoop: true,
            directionNav: false,
            slideshow: true,
            slideshowSpeed: 5000
        });
    </script>



