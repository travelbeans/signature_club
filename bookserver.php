<?php
session_start();

// initializing variables
$username = "";
$membership_type = "";
$destination = "";
$hotel_name = "";
$booking_date = "";
$guest_name = "";

$errors = array(); 

// connect to the database
$db = mysqli_connect('localhost', 'root', '8804', 'registration');

// REGISTER USER
if (isset($_POST['book'])) {
  // receive all input values from the form
  $username = mysqli_real_escape_string($db, $_POST['username']);
  $membership_type = mysqli_real_escape_string($db, $_POST['membership_type']);
  $destination = mysqli_real_escape_string($db, $_POST['destination']);
  $hotel_name = mysqli_real_escape_string($db, $_POST['hotel_name']);
  $booking_date = mysqli_real_escape_string($db, $_POST['booking_date']);
  $guest_name = mysqli_real_escape_string($db, $_POST['guest_name']);


  // form validation: ensure that the form is correctly filled ...
  // by adding (array_push()) corresponding error unto $errors array
  if (empty($username)) { array_push($errors, "Username is required"); }
  if (empty($destination)) { array_push($errors, "Destination is required"); }
  if (empty($hotel_name)) { array_push($errors, "Hotel Name is required"); }
  if (empty($booking_date)) { array_push($errors, "Booking Date is required"); }
  if (empty($guest_name)) { array_push($errors, "Guest name is required"); }
  
  
  // Finally, register user if there are no errors in the form
 
  if (count($errors) == 0) {
  	$query = "INSERT INTO booking (username, membership_type, destination, hotel_name, booking_date, guest_name) 
  			  VALUES('$username', '$membership_type','$destination', '$hotel_name', '$booking_date', '$guest_name' )";
    mysqli_query($db, $query);
  	$_SESSION['success'] = "You are now logged in";
    header('location: booking.php');
}
}

?>
