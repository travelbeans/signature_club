<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Swanky+and+Moo+Moo&display=swap" rel="stylesheet">
    <title>Member List</title>
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }
        td, th {
            border: 1px solid #ddd;
            text-align: left;
            padding: 8px;
        }
        tr:nth-child(even) {
            background-color: #ddd;
        }
        h1 {
            font-family: 'Swanky and Moo Moo', cursive;
            align: center;
            font-size: 60px;
        }
    </style>
   
</head>
<body>

<h1>Member's List</h1>
    <table>
        <tr>
            <th>Sr. No.</th>
            <th>Member Name</th>
            <th>Member Id</th>
            <th>Password</th>
            <th>Total Night</th>
            <th>Night Remaining</th>
            <th>DOB</th>
            <th>Gender</th>
            <th>Address</th>
            <th>Email</th>
            <th>Phone</th>
        </tr>


        <?php
    $con = mysqli_connect("localhost", "root", "8804", "membership");
    if ($con-> connect_error) {
    die("Connection Failed:". $con-> connect_error);
    }

    $sql = "SELECT id, mname, membernum, mpassword, snight, rnight, dob, gender, maddress, email, mphone FROM member";
    $result = $con-> query($sql);

    if ($result-> num_rows > 0) {
        while ($row = $result-> fetch_assoc()) {
            echo "<tr><td>". $row["id"] ."</td><td>". $row["mname"] ."</td><td>". $row["membernum"] ."</td><td>". $row["mpassword"] ."</td><td>". $row
            ["snight"] ."</td><td>". $row["rnight"] ."</td><td>". $row["dob"] ."</td><td>". $row["gender"] ."</td><td>". $row
            ["maddress"] ."</td><td>". $row["email"] ."</td><td>". $row["mphone"] ."</td></tr>";
        }
        echo "</table>";
    }
    else {
        echo "0 result";
    }

    $con-> close();
?>
    </table>
</body>
</html>

