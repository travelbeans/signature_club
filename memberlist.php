<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Member's List</title>
   
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css?family=Swanky+and+Moo+Moo&display=swap" rel="stylesheet">

    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
            background:
        }
        td, th {
            border: 1px solid #ddd;
            text-align: center;
            padding: 5px;
            text-align: center;
        }
        tr:nth-child(even) {
            background-color: #e6e6e6;
        }
        tr:hover{
            background-color: #f5f5f5;
        }
        h1 {
            font-family: 'Swanky and Moo Moo', cursive;
            align: center;
            font-size: 60px;
            color: #ff5005;
        }
    </style>

    
</head>
<body>
        <?php
        session_start();
        include('header.php');
        $con = mysqli_connect("localhost", "root", "8804", "registration");

        $total = mysqli_query($con, "SELECT * FROM users");
        $no_of_rec = mysqli_num_rows($total);

        $start = 0;
        $limit = 20;


        if(isset($_GET['id'])){
            $id=$_GET['id'];
            $start = ($id-1) * $limit;
        }

        else{
            $id=1;
        }

        $page = ceil($no_of_rec/$limit);
        $query = mysqli_query($con, "SELECT * FROM users limit $start, $limit");
        ?>
        <h1 style="font-family: 'Swanky and Moo Moo', cursive; font-size: 44px; text-align: center;">Member's List</h1>
        <hr>      

        <table>
            <thead>
                <tr>
                    <th>Sr. <br> No.</th>
                    <th>Member <br> Name</th>
                    <th>Type</th>
                    <th>Member <br> Id</th>
                    <th>Password</th>
                    <th>Total <br> Night</th>
                    <th>Night <br> Remaining</th>
                    <th>DOB</th>
                    <th>Gender</th>
                    <th>Address</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Bookings</th>
                </tr>
            </thead>

            <tbody>
                <?php
                while($datafetch = mysqli_fetch_array($query))
                {?>
                    <tr>
                        <td><?= $datafetch['0']?></td>
                        <td><?= $datafetch['1']?></td>
                        <td><?= $datafetch['2']?></td>
                        <td><?= $datafetch['3']?></td>
                        <td><?= $datafetch['4']?></td>
                        <td><?= $datafetch['5']?></td>
                        <td><?= $datafetch['6']?></td>
                        <td><?= $datafetch['7']?></td>
                        <td><?= $datafetch['8']?></td>
                        <td><?= $datafetch['9']?></td>
                        <td><?= $datafetch['10']?></td>
                        <td><?= $datafetch['11']?></td>
                        <td><a style="background-color: #ff5005; color: white; padding: 5px 20px; text-decoration: none; display: inline-block;"
                             href="bookinglist.php?username=<?php echo $datafetch['3'] ?>">Show Bookings</a></td>
                        

                    </tr>
                <?php
                }
                ?>
            </tbody>
        </table>
        
    </div>
    <br>
    <div class="container">
        <ul class="pagination"> 
            <?php
            if($id > 1){
                ?>

            <li class="page-item"><a class="page-link" href="?id=<?php echo ($id-1); ?> ">Previous</a></li>
            <?php
             } 
            ?>

            <?php
                for($i=1;$i <= $page;$i++){?>
                   <li class="page-item"><a class="page-link" href="?id=<?php echo $i; ?> "><?php echo $i; ?></a></li>     
                <?php
                }
            ?>
            <?php
            if($id!=$page){?>

            <li class="page-item"><a class="page-link" href="?id=<?php echo ($id+1); ?> ">Next</a></li>
            <?php
            }    
            ?>
        </ul>
    </div>

    <?php echo $datafetch['2']; ?>
    
</body>
</html>