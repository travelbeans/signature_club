<?php
session_start();

// initializing variables
$name = "";
$username = "";
$tnight = "";
$rnight = "";
$dob = "";
$gender = "";
$address = "";
$email = "";
$phone = "";

$email    = "";
$errors = array(); 

// connect to the database
$db = mysqli_connect('localhost', 'root', '8804', 'registration');

// REGISTER USER
if (isset($_POST['reg_user'])) {
  // receive all input values from the form
  $name = mysqli_real_escape_string($db, $_POST['name']);
  $membership_type = mysqli_real_escape_string($db, $_POST['membership_type']);
  $username = mysqli_real_escape_string($db, $_POST['username']);
  $password_1 = mysqli_real_escape_string($db, $_POST['password_1']);
  $tnight = mysqli_real_escape_string($db, $_POST['tnight']);
  $rnight = mysqli_real_escape_string($db, $_POST['rnight']);
  $dob = mysqli_real_escape_string($db, $_POST['dob']);
  $gender = mysqli_real_escape_string($db, $_POST['gender']);
  $address = mysqli_real_escape_string($db, $_POST['address']);
  $email = mysqli_real_escape_string($db, $_POST['email']);
  $phone = mysqli_real_escape_string($db, $_POST['phone']);


  // form validation: ensure that the form is correctly filled ...
  // by adding (array_push()) corresponding error unto $errors array
  if (empty($name)) { array_push($errors, "Member Name is required"); }
  if (empty($membership_type)) { array_push($errors, "Member Type is required"); }
  if (empty($username)) { array_push($errors, "Username is required"); }
  if (empty($password_1)) { array_push($errors, "Password is required"); }
  if (empty($tnight)) { array_push($errors, "Please enter Total Nights"); }
  if (empty($rnight)) { array_push($errors, "Please enter Remaining Nights"); }
  if (empty($dob)) { array_push($errors, "Date of Birth is required"); }
  if (empty($gender)) { array_push($errors, "Gender is required"); }
  if (empty($address)) { array_push($errors, "Address is required"); }
  if (empty($email)) { array_push($errors, "Email is required"); }
  if (empty($phone)) { array_push($errors, "Phone No. is required"); }
  
  

  // first check the database to make sure 
  // a user does not already exist with the same username and/or email
  $user_check_query = "SELECT * FROM users WHERE username='$username' OR email='$email' OR phone='$phone' LIMIT 1";
  $result = mysqli_query($db, $user_check_query);
  $user = mysqli_fetch_assoc($result);
  
  if ($user) { // if user exists
    if ($user['username'] === $username) {
      array_push($errors, "Member ID already exists");
    }

    if ($user['email'] === $email) {
      array_push($errors, "Email already exists");
    }
    if ($user['phone'] === $phone) {
        array_push($errors, "Phone number already exists");
      }
  }

  // Finally, register user if there are no errors in the form
  if (count($errors) == 0) {
  	$password = ($password_1);//encrypt the password before saving in the database

  	$query = "INSERT INTO users (name, membership_type, username, password, tnight, rnight, dob, gender, address, email, phone ) 
  			  VALUES('$name','$membership_type','$username','$password','$tnight','$rnight','$dob','$gender','$address','$email','$phone' )";
    mysqli_query($db, $query);
    $_SESSION['username'] = $username;
  	$_SESSION['success'] = "You are now logged in";
    header("Inserted successfully");
    
  }
}


if (isset($_POST['login_user'])) {
  $username = mysqli_real_escape_string($db, $_POST['username']);
  $password = mysqli_real_escape_string($db, $_POST['password']);

  if (empty($username)) {
  	array_push($errors, "Username is required");
  }
  if (empty($password)) {
  	array_push($errors, "Password is required");
  }

  if (count($errors) == 0) {
  	$password = ($password);
  	$query = "SELECT * FROM users WHERE username='$username' AND password='$password'";
  	$results = mysqli_query($db, $query);
  	if (mysqli_num_rows($results) == 1) {
      $_SESSION['username'] = $username;
  	  $_SESSION['success'] = header('location: partnerdashboard.php');
  	}else {
  		array_push($errors, "Wrong username/password combination");
  	}
  }
}

?>
<style>
  h5{
    color: red;
  }
  </style>