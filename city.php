<?php 
session_start(); 
?>
<!DOCTYPE html>
<html class=" js flexbox canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers no-applicationcache svg inlinesvg smil svgclippaths"><!--<![endif]--><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Page Title -->
    <title>Delhi | Signature Club Memberships | Travel Travel Square</title>
    
    <!-- Meta Tags -->
    
    <meta name="keywords" content="HTML5 Template">
    <meta name="description" content="Travelo - Travel, Tour Booking HTML5 Template">
    <meta name="author" content="SoapTheme">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="#" type="image/x-icon">
    
    <!-- Theme Styles -->
    <link rel="stylesheet" href="./cities_files/bootstrap.min.css">
    <link rel="stylesheet" href="./cities_files/font-awesome.min.css">
    <link href="./cities_files/css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="./cities_files/animate.min.css">
    
    <!-- Main Style -->
    <!--<link id="main-style" rel="stylesheet" href="css/style.css">-->
    <script type="text/javascript">
        var mainStyle = "style.css";
        if (typeof localStorage != "undefined") {
            var colorSkin = localStorage.getItem("colorSkin");
            if (colorSkin != null) {
                mainStyle = "style-" + colorSkin + ".css";
            }
        }
        document.write('<link id="main-style" rel="stylesheet" href="css/' + mainStyle + '">');
    </script><link id="main-style" rel="stylesheet" href="./cities_files/style.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <!-- Updated Styles -->
    <link rel="stylesheet" href="./cities_files/updates.css">

    <!-- Custom Styles -->
    <link rel="stylesheet" href="./cities_files/custom.css">
    
    <!-- Responsive Styles -->
    <link rel="stylesheet" href="./cities_files/responsive.css">
    
    <!-- CSS for IE -->
    <!--[if lte IE 9]>
        <link rel="stylesheet" type="text/css" href="css/ie.css" />
    <![endif]-->
    
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script type='text/javascript' src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
      <script type='text/javascript' src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
    <![endif]-->
</head>
<body>
    <div id="page-wrapper">
    <?php include('php_session.php'); ?>


<header id="header" class="navbar-static-top">
    <div class="topnav hidden-xs">
        <div class="container">
            <ul class="quick-menu pull-left">
                <li><a href="http://www.thetravelsqaure.com">THE TRAVEL SQAURE</a></li>
                <li><a href="http://www.travelbeans.in">TRAVEL BEANS</a></li>
            </ul>
            <ul class="quick-menu pull-right">
            <li><a href="login.php">Login</a></li>
                <li class="ribbon">
                    <a href=""><?php
                     if($_SESSION["username"] == true){ echo "Hi! "." ".htmlspecialchars($row['name']); }
                      ?></a><i class="fa fa-angle-down"></i>
                    <ul class="menu mini uppercase left">
                        <li><a href="partnerdashboard.php" class="location-reload">Dashboard</a></li>
                        <li><a href="logout.php">Signout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
            
            <div class="main-header">
                
                <a href="#" data-toggle="collapse" class="mobile-menu-toggle">
                    Mobile Menu Toggle
                </a>

                <div class="container">
                    <h1 class="navbar-brand" style="height: 20%;">
                        <a href="index.php" title="Travelo - home">
                            <img src="logo.jpg" alt="Travelo HTML5 Template">
                        </a>
                    </h1>
                    
                    <nav id="main-menu" role="navigation">
                        <ul class="menu">
                            <li class="menu-item-has-children">
                                <a href="index.php">Home</a>
                            </li>
							<li class="menu-item-has-children">
                                <a href="about.php">About Signature Club</a>
                            </li>
							<li class="menu-item-has-children">
                                <a href="destinations.php">Worldwide Destinations</a>
                            </li>
                            <li class="menu-item-has-children">
                                        <a href="#">Membership Plans</a>
                                        <ul>
                                            <li><a href="premium.php">Premium Membership</a></li>
                                            <li><a href="prestige.php">Prestige Membership</a></li>
                                            <li><a href="privilege.php">Privilege Membership</a></li>
                                        </ul>
                                    </li>
							<li class="menu-item-has-children">
                                <a href="travelsquare.in">Contact Us</a>
                            </li>
                           
                            
                            
                        </ul>
                    </nav>
                </div>
                
                <nav id="mobile-menu-01" class="mobile-menu collapse">
                    <ul id="mobile-primary-menu" class="menu">
                        <li class="menu-item-has-children">
                            <a href="index.php">Home</a><button class="dropdown-toggle collapsed" data-toggle="collapse" data-target="#mobile-menu-submenu-item-0"></button>
                           
                        </li>
                        <li class="menu-item-has-children">
                            <a href="#">Membership Plans</a><button class="dropdown-toggle collapsed" data-toggle="collapse" data-target="#mobile-menu-submenu-item-1"></button>
                            <ul id="mobile-menu-submenu-item-1" class="collapse">
                                <li><a href="premium.php">Premium Membership</a></li>
                                <li><a href="prestige.php">Prestige Membership</a></li>
                                <li><a href="privilege.php">Privilege Membership</a></li>
                            </ul>
                        </li>
                    </ul>
                        
                    
                    <ul class="mobile-topnav container">
                        <li><a href="partnerdashboard.php">MY ACCOUNT</a></li>
                       
                        <li><a href="login.php" class="soap-popupbox">LOGIN</a></li>
                        
                       
                    </ul>
                    
                </nav>
            </div>
            
            
        </header>

        <section id="content" class="tour">
            <div id="slideshow" class="slideshow-bg full-screen" style="height: 550px;">
                <div class="flexslider">
                    <ul class="slides">
                        <li style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;" class="">
                            <div class="slidebg" style="background-image: url(images/akshardham-mandir-the-travel-sqaure-signature-club.jpg);"></div>
                        </li>
                        <li class="" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;">
                            <div class="slidebg" style="background-image: url(images/India-Gate-The-Travel-Square-Signature-Club.jpg);"></div>
                        </li>
                        <li style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;" class="flex-active-slide">
                            <div class="slidebg" style="background-image: url(images/qutub-minar-the-travel-square-signature-club.jpg);"></div>
                        </li>
                    </ul>
                </div>
                <div class="container">
                    <div class="table-wrapper full-width">
                        <div class="table-cell">
                        </div>
                    </div>
                </div>
            </div>
            <div class="section white-bg">
                <div class="container">
                    <div class="text-center description block">
                        <h1>Premium Properties in Delhi</h1>
                        <p>We Have A Variety of Luxury Accomodation Facilities & Properties In Delhi</p>
                    </div>
                    <div class="tour-packages row add-clearfix image-box">
                        <div class="col-sm-6 col-md-4">
                            <article class="box animated fadeInLeft" data-animation-type="fadeInLeft" style="animation-duration: 1s; visibility: visible;">
                                <figure>
                                    <a href="#"><img src="images/royalplaza.jpg" alt=""></a>
                                    <figcaption>
                                        <span class="price"></span>
                                        <h2 class="caption-title">Hotel Royal Plaza</h2>
                                    </figcaption>
                                </figure>
                            </article>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <article class="box animated fadeInDown" data-animation-type="fadeInDown" style="animation-duration: 1s; visibility: visible;">
                                <figure>
                                    <a href="#"><img src="images/prideplaza.jpg" alt=""></a>
                                    <figcaption>
                                        <span class="price"></span>
                                        <h2 class="caption-title">Pride Plaza</h2>
                                    </figcaption>
                                </figure>
                            </article>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <article class="box animated fadeInRight" data-animation-type="fadeInRight" style="animation-duration: 1s; visibility: visible;">
                                <span class="discount"><span class="discount-text"></span></span>
                                <figure>
                                    <a href="#"><img src="images/lemontree.jpg" alt=""></a>
                                    <figcaption>
                                        <span class="price"></span>
                                        <h2 class="caption-title">Lemon Tree Premier</h2>
                                    </figcaption>
                                </figure>
                            </article>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <article class="box animated fadeInLeft" data-animation-type="fadeInLeft" style="animation-duration: 1s; visibility: visible;">
                                <figure>
                                    <a href="#"><img src="images/roseatehouse.jpg" alt=""></a>
                                    <figcaption>
                                        <span class="price"></span>
                                        <h2 class="caption-title">Roseate House</h2>
                                    </figcaption>
                                </figure>
                            </article>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <article class="box animated fadeInUp" data-animation-type="fadeInUp" style="animation-duration: 1s; visibility: visible;">
                                <span class="discount"><span class="discount-text"></span></span>
                                <figure>
                                    <a href="#"><img src="images/parkinn.jpg" alt=""></a>
                                    <figcaption>
                                        <span class="price"></span>
                                        <h2 class="caption-title">ParkInn By Radisson</h2>
                                    </figcaption>
                                </figure>
                            </article>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <article class="box animated fadeInUp" data-animation-type="fadeInUp" style="animation-duration: 1s; visibility: visible;">
                                <span class="discount"><span class="discount-text"></span></span>
                                <figure>
                                    <a href="#"><img src="images/theumrao.jpg" alt=""></a>
                                    <figcaption>
                                        <span class="price"></span>
                                        <h2 class="caption-title">The Umrao</h2>
                                    </figcaption>
                                </figure>
                            </article>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <article class="box animated fadeInUp" data-animation-type="fadeInUp" style="animation-duration: 1s; visibility: visible;">
                                <span class="discount"><span class="discount-text"></span></span>
                                <figure>
                                    <a href="#"><img src="images/fourpointsbysheraton.jpg" alt=""></a>
                                    <figcaption>
                                        <span class="price"></span>
                                        <h2 class="caption-title">Four Points By Sheraton</h2>
                                    </figcaption>
                                </figure>
                            </article>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <article class="box animated fadeInUp" data-animation-type="fadeInUp" style="animation-duration: 1s; visibility: visible;">
                                <span class="discount"><span class="discount-text"></span></span>
                                <figure>
                                    <a href="#"><img src="images/parkplaza.jpg" alt=""></a>
                                    <figcaption>
                                        <span class="price"></span>
                                        <h2 class="caption-title">Park Plaza CBD</h2>
                                    </figcaption>
                                </figure>
                            </article>
                        </div>
                    </div>
                </div>
            </div>           
        </section>
        <hr>
        
        <footer id="footer" class="style5">
            <div class="footer-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <h2>The Travel Square</h2>
                            <ul class="discover triangle hover row">
                                <li class="col-xs-6"><a href="http://www.thetravelsquare.in/about-Travel-Square.html">About Company</a></li>
                                <li class="col-xs-6"><a href="http://www.thetravelsquare.in/jobs.html">Jobs & Carreers</a></li>
								<li class="col-xs-6"><a href="http://www.thetravelsquare.in/about-Travel-Square.html">Corporate Offices</a></li>
                                <li class="col-xs-6"><a href="http://www.thetravelsquare.in/legal.html">Legal Information</a></li>
                                <li class="col-xs-6"><a href="http://www.thetravelsquare.in/press.html">Press Release</a></li>
                                <li class="col-xs-6"><a href="http://www.thetravelsquare.in/legal.html">T & C</a></li>
                                <li class="col-xs-6"><a href="http://www.thetravelsquare.in/customer-care.html">Customer Care</a></li>
                                <li class="col-xs-6"><a href="http://www.thetravelsquare.in/legal.html">Privacy Policies</a></li>
                                <li class="col-xs-6"><a href="http://www.thetravelsquare.in/partner-Care.html">Partner Help</a></li>
                                <li class="col-xs-6"><a href="http://www.thetravelsquare.in/faq.html">FAQ's</a></li>
                                <li class="col-xs-6"><a href="http://www.thetravelsquare.in/site-Map.html">Site Map</a></li>
                                <li class="col-xs-6"><a href="http://www.thetravelsquare.in/contact-Us.html">Contact Us</a></li>
                            </ul>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <h2>Our Products</h2>
                            <ul class="discover triangle hover row">
                                <li class="col-xs-"><a href="http://www.thetravelsquare.in/pro">Travel Square Business</a></li>
                                <li class="col-xs-"><a href="http://www.signatureclubmembership.com/">Signature Club</a></li>
                                <li class="col-xs-"><a href="http://www.cruisader.com/">Cruisader</a></li>
                                <li class="col-xs-"><a href="http://www.cruisingindia.in/">Cruising India</a></li>
                                <li class="col-xs-"><a href="http://www.happenings.ind.in/">Happenings</a></li>
                                <li class="col-xs-"><a href="http://www.shoppist.me/">Shoppist</a></li>
                                
                            </ul>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <img src="images/Signature-Club-Logo.jpg">
                            <br>
                            
                            <br>
                            
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <h2>About Us</h2>
                            <p>Signature Club Founded in 2018 , Aims to Provide Complete Luxury During the Journey of Our Members . The Club Offers More than 75+ Destinations With 200+ Hotels , 
	Worldwide Covering Most of the Travel Destinations in the World .</p>
                            <br>
                            <address class="contact-details">
                                <center><a href="" class="contact-email">signatureclubmembership@gmail.com</a></center>
                            </address>
                            <ul class="social-icons clearfix">
                                <li class="twitter"><a title="Join Us On Twitter" href="https://twitter.com/signaturemember" data-toggle="tooltip" data-original-title="twitter" target="blank_"><i class="soap-icon-twitter"></i></a></li>
                                <li class="facebook"><a title="Join Us On Facebook" href="https://www.facebook.com/signatureclubmembership/" target="blank_" data-toggle="tooltip" data-original-title="facebook"><i class="soap-icon-facebook"></i></a></li>
                                <li class="instagram"><a title="Join Us On Instagram" href="https://www.instagram.com/signatureclubmembership/" target="blank_" data-toggle="tooltip" data-original-title="facebook"><i class="soap-icon-instagram"></i></a></li>
                                <li class="linkedin"><a title="Join Us On Linked In" href="" target="blank_" data-toggle="tooltip" data-original-title="linkedin"><i class="soap-icon-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottom gray-area">
                <div class="container">
                    
                    <div class="pull-right">
                        <a id="back-to-top" href="" class="animated bounce" data-animation-type="bounce" style="animation-duration: 1s; visibility: visible;"><i class="soap-icon-longarrow-up circle"></i></a>
                    </div>
                    <div class="copyright pull-right">
                        <p>© 2019 Signature Club | The Travel Square</p>
                    </div>
                </div>
            </div>
        </footer>
    </div>


    <!-- Javascript -->
    <script type="text/javascript" src="./Hotel Collections_files/jquery-1.11.1.min.js.download"></script>
    <script type="text/javascript" src="./Hotel Collections_files/jquery.noconflict.js.download"></script>
    <script type="text/javascript" src="./Hotel Collections_files/modernizr.2.7.1.min.js.download"></script>
    <script type="text/javascript" src="./Hotel Collections_files/jquery-migrate-1.2.1.min.js.download"></script>
    <script type="text/javascript" src="./Hotel Collections_files/jquery.placeholder.js.download"></script>
    <script type="text/javascript" src="./Hotel Collections_files/jquery-ui.1.10.4.min.js.download"></script>
    
    <!-- Twitter Bootstrap -->
    <script type="text/javascript" src="./Hotel Collections_files/bootstrap.js.download"></script>

    <!-- Flex Slider -->
    <script type="text/javascript" src="./Hotel Collections_files/jquery.flexslider-min.js.download"></script>
    
    <!-- parallax -->
    <script type="text/javascript" src="./Hotel Collections_files/jquery.stellar.min.js.download"></script>
    
    <!-- waypoint -->
    <script type="text/javascript" src="./Hotel Collections_files/waypoints.min.js.download"></script>

    <!-- load page Javascript -->
    <script type="text/javascript" src="./Hotel Collections_files/theme-scripts.js.download"></script>
    <script type="text/javascript" src="./Hotel Collections_files/scripts.js.download"></script>

    <script type="text/javascript">
        tjq("#slideshow .flexslider").flexslider({
            animation: "fade",
            controlNav: false,
            animationLoop: true,
            directionNav: false,
            slideshow: true,
            slideshowSpeed: 5000
        });
    </script>



