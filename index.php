<?php session_start(); ?>
<!DOCTYPE html>

	<html>
	<head>
	
    <!-- Page Title -->
    <title>Signature Club Memberships | The Travel Square</title>
    
    <!-- Meta Tags -->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="author" content="Travel Beans">
    <meta name="description" content="">
    <meta name="distribution" content="global">
    <meta name="country" content="india">
    <meta name="copyright" content="Travel Beans">
    <meta name="revisit-after" content="1 hour">
    <meta name="designer" content="Travel Beans">
    <meta name="language" content="english">
    <meta name="robots" content="index, follow">
    <meta name="rating" content="general">
    <meta name="publisher" content="Travel Beans">

    <meta property="og:title" content="Signature Club | Travel Beans">
    <meta property="og:description" content="Signature Club Founded and Managed By Travel Beans (Good Vibes Travel Ventures Pvt. Ltd.) Group Was Established in 2018 .
	It Aims to Provide Luxury Properties at Affordable Prices to it's Members & Affiliates . The Club Offers More than 75+ Destinations With 200+ Hotels , 
	Worldwide Covering Most of the Travel Destinations in the World The Membership Offers to Standardise the Accomodation of It's Members to Have Leisure 
	Comfortable Feel While Travelling to Different Places or For Holidays .">
    <meta property="og:image" content="">
  
    <meta name="twitter:card" content="">
  
    <meta name="keywords" content="">
    <meta name="format-detection" content="telephone=no">
	
    
    <!-- Theme Styles -->
	<link rel="shortcut icon" href="" type="image/x-icon">
    <link rel="stylesheet" href="./Hotel Collections_files/bootstrap.min.css">
    <link rel="stylesheet" href="./Hotel Collections_files/font-awesome.min.css">
    <link rel="stylesheet" href="./Hotel Collections_files/animate.min.css">
	
    <link href="./Hotel Collections_files/css" rel="stylesheet" type="text/css">
    
    <!-- Main Style -->
	<style>
	.blink 
	{
	animation: blinker 1s linear infinite;
	}

	@keyframes blinker 
	{  
	75% { opacity: 0.0; }
	}
	</style>
	
    <script type="text/javascript">
        var mainStyle = "style.css";
        if (typeof localStorage != "undefined") {
            var colorSkin = localStorage.getItem("colorSkin");
            if (colorSkin != null) {
                mainStyle = "style-" + colorSkin + ".css";
            }
        }
        document.write('<link id="main-style" rel="stylesheet" href="css/' + mainStyle + '">');
    </script>
	
    <link id="main-style" rel="stylesheet" href="./Hotel Collections_files/style-light-yellow.css">
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <!-- Updated Styles -->
    <link rel="stylesheet" href="./Hotel Collections_files/updates.css">

    <!-- Custom Styles -->
    <link rel="stylesheet" href="./Hotel Collections_files/custom.css">
    
    <!-- Responsive Styles -->
    <link rel="stylesheet" href="./Hotel Collections_files/responsive.css">

	</head>

<body>
    <div id="page-wrapper">
        
    <?php
                    
                    $a = $_SESSION["username"];
                    $db = mysqli_connect('localhost', 'root', '8804', 'registration');
                    $query = "SELECT * FROM users WHERE username ='$a'";
                    $show = mysqli_query($db, $query);
                    $row = $show -> fetch_assoc();
                    $_SESSION['row'] = $row; 
                    ?>
        
		
                <header id="header" class="navbar-static-top">
                    <div class="topnav hidden-xs">
                        <div class="container">
                            <ul class="quick-menu pull-left">
                                <li><a href="http://www.thetravelsqaure.com">THE TRAVEL SQAURE</a></li>
                                <li><a href="http://www.travelbeans.in">TRAVEL BEANS</a></li>
                            </ul>
                            <ul class="quick-menu pull-right">
                            <li><a href="login.php">Login</a></li>
                                <li class="ribbon">
                                    <a href=""><?php if($_SESSION["username"] == true){ echo "Hi! "." ".htmlspecialchars($row['name']); } ?></a><i class="fa fa-angle-down"></i>
                                    <ul class="menu mini uppercase left">
                                        <li><a href="partnerdashboard.php" class="location-reload">Dashboard</a></li>
                                        <li><a href="logout.php">Signout</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
            
            <div class="main-header">
                
                <a href="http://www.soaptheme.net/html/travelo/dashboard1.html#mobile-menu-01" data-toggle="collapse" class="mobile-menu-toggle">
                    Mobile Menu Toggle
                </a>

                <div class="container">
                    <h1 class="logo navbar-brand">
                        <a href="http://www.thetravelsqaure.com" title="The Travel Sqaure">
                            <img src="logo.jpg" alt="The Travel Sqaure by Travel Beans">
                        </a>
                    </h1>
                    
                    <nav id="main-menu" role="navigation">
                        <ul class="menu">
                            <li class="menu-item-has-children">
                                <a href="index.php">Home</a>
                            </li>
							<li class="menu-item-has-children">
                                <a href="about.php">About Signature Club</a>
                            </li>
							<li class="menu-item-has-children">
                                <a href="destinations.php">Worldwide Destinations</a>
                            </li>
                            <li class="menu-item-has-children">
                                        <a href="#">Membership Plans</a>
                                        <ul>
                                            <li><a href="premium.php">Premium Membership</a></li>
                                            <li><a href="prestige.php">Prestige Membership</a></li>
                                            <li><a href="privilege.php">Privilege Membership</a></li>
                                        </ul>
                                    </li>
							<li class="menu-item-has-children">
                                <a href="travelsquare.in">Contact Us</a>
                            </li>
                           
                            
                            
                        </ul>
                    </nav>
                </div>
                
                <nav id="mobile-menu-01" class="mobile-menu collapse">
                    <ul id="mobile-primary-menu" class="menu">
                        <li class="menu-item-has-children">
                            <a href="http://www.signatureclubmembership.com/">Home</a><button class="dropdown-toggle collapsed" data-toggle="collapse" data-target="#mobile-menu-submenu-item-0"></button>
                            
                        </li>
                        <li class="menu-item-has-children">
                            <a href="">Hotels</a><button class="dropdown-toggle collapsed" data-toggle="collapse" data-target="#mobile-menu-submenu-item-2"></button>
                        </li>
                       
                        
                            
                        
                        
                    </ul>
                    
                    <ul class="mobile-topnav container">
                        <li><a href="partnerdashboard.php">MY ACCOUNT</a></li>
                        
                        <li><a href="login.php" class="soap-popupbox">LOGIN</a></li>
                    </ul>
                    
                </nav>
				
				
            </div>
            <div id="travelo-signup" class="travelo-signup-box travelo-box">
                <div class="login-social">
                    <a href="http://www.soaptheme.net/html/travelo/dashboard1.html#" class="button login-facebook"><i class="soap-icon-facebook"></i>Login with Facebook</a>
                    <a href="http://www.soaptheme.net/html/travelo/dashboard1.html#" class="button login-googleplus"><i class="soap-icon-googleplus"></i>Login with Google+</a>
                </div>
                <div class="seperator"><label>OR</label></div>
                <div class="simple-signup">
                    <div class="text-center signup-email-section">
                        <a href="http://www.soaptheme.net/html/travelo/dashboard1.html#" class="signup-email"><i class="soap-icon-letter"></i>Sign up with Email</a>
                    </div>
                    <p class="description">By signing up, I agree to Travelo's Terms of Service, Privacy Policy, Guest Refund olicy, and Host Guarantee Terms.</p>
                </div>
                <div class="email-signup">
                    <form>
                        <div class="form-group">
                            <input type="text" class="input-text full-width" placeholder="first name">
                        </div>
                        <div class="form-group">
                            <input type="text" class="input-text full-width" placeholder="last name">
                        </div>
                        <div class="form-group">
                            <input type="text" class="input-text full-width" placeholder="email address">
                        </div>
                        <div class="form-group">
                            <input type="password" class="input-text full-width" placeholder="password">
                        </div>
                        <div class="form-group">
                            <input type="password" class="input-text full-width" placeholder="confirm password">
                        </div>
                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox"> Tell me about Travelo news
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <p class="description">By signing up, I agree to Travelo's Terms of Service, Privacy Policy, Guest Refund Policy, and Host Guarantee Terms.</p>
                        </div>
                        <button type="submit" class="full-width btn-medium">SIGNUP</button>
                    </form>
                </div>
                <div class="seperator"></div>
                <p>Already a Travelo member? <a href="http://www.soaptheme.net/html/travelo/dashboard1.html#travelo-login" class="goto-login soap-popupbox">Login</a></p>
            </div>
            <div id="travelo-login" class="travelo-login-box travelo-box">
                <div class="login-social">
                    <a href="http://www.soaptheme.net/html/travelo/dashboard1.html#" class="button login-facebook"><i class="soap-icon-facebook"></i>Login with Facebook</a>
                    <a href="http://www.soaptheme.net/html/travelo/dashboard1.html#" class="button login-googleplus"><i class="soap-icon-googleplus"></i>Login with Google+</a>
                </div>
                <div class="seperator"><label>OR</label></div>
                <form>
                    <div class="form-group">
                        <input type="text" class="input-text full-width" placeholder="email address">
                    </div>
                    <div class="form-group">
                        <input type="password" class="input-text full-width" placeholder="password">
                    </div>
                    <div class="form-group">
                        <a href="http://www.soaptheme.net/html/travelo/dashboard1.html#" class="forgot-password pull-right">Forgot password?</a>
                        <div class="checkbox checkbox-inline">
                            <label>
                                <input type="checkbox"> Remember me
                            </label>
                        </div>
                    </div>
                </form>
                <div class="seperator"></div>
                <p>Don't have an account? <a href="http://www.soaptheme.net/html/travelo/dashboard1.html#travelo-signup" class="goto-signup soap-popupbox">Sign up</a></p>
            </div>
        </header>

        <section id="content" class="tour">
            <div id="slideshow" class="slideshow-bg" style="height: 350px;">
                <div class="flexslider">
                    <ul class="slides">
                        <li class="" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; transition: opacity 0.6s ease; z-index: 1;">
                            <div class="slidebg" style="background-image: url(images/Trave.jpg);"></div>
                        </li>
                        <li class="" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; transition: opacity 0.6s ease; z-index: 1;">
                            <div class="slidebg" style="background-image: url(images/Travel-Beans-Family-Membership-Hotels.jpg);"></div>
                        </li>
                        
                        
                    </ul>
                </div>
                <div class="container">
                    <div class="table-wrapper full-width">
                        <div class="table-cell">
                            
                            
                            
                        </div>
                    </div>
                </div>
				
            </div>
			
            <div class="section white-bg">
                <div class="container">
                    <div class="text-center description block"><center>
							
                        <h1>Lucrative & Hassle Free Holiday Membership Plans</h1>
						<div class="text-center"><br>
                                <a href="Membership-Destinations.html" class="button btn-medium dark-orange uppercase animated" data-animation-type="shake">View All Destinations & Properties</a>
                            </div>
                    </div>
                    <div id="collections" class="block">
                    
                    <div class="row image-box style4">
						<div class="col-sm-4">
                            <article class="box">
                                <figure>
                                    <a class="hover-effect" href="premium.php" title="">
									<img src="images/1-Year-Luxury-Membership-Plan-Travel-Beans.jpg" alt="" width="370" height="172"></a>
                                </figure>
                                <div class="details">
                                    <h4 class="box-title"><b>Premium</b> | 1 Yr Luxury Membership Plan</h4>
                                    <a href="premium.php" class="goto-detail"></a>
                                </div>
                            </article>
                        </div>
                        <div class="col-sm-4">
                            <article class="box">
                                <figure>
                                    <a class="hover-effect" href="privilege.php" title="">
									<img src="images/3-Years-Luxury-Family-Membership-Plan-Travel-Beans.jpg" alt="" width="370" height="172"></a>
                                </figure>
                                <div class="details">
                                    <h4 class="box-title"><b>Privilege</b> : 3 Yrs Family Membership Plan</h4>
                                    <a href="privilege.php" class="goto-detail"></a>
                                </div>
                            </article>
                        </div>
						<div class="col-sm-4">
                            <article class="box">
                                <figure>
                                    <a class="hover-effect" href="prestige.php" title="">
									<img src="images/5-Years-Luxury-Traveller-Exclusive-Membership-Plan-Travel-Beans.jpg" alt="" width="370" height="172"></a>
                                </figure>
                                <div class="details">
                                    <h4 class="box-title"><b>Prestige</b> : 5 Yrs Travellers Exclusive</h4>
                                    <a href="prestige.php" class="goto-detail"></a>
                                </div>
                            </article>
                        </div>
						
						
                    </div>
                </div>
                </div>
            </div>
			
			
            <div class="section">
                <div class="container">
                    <h2>Membership Services & Offers</h2>
                    <div class="tour-guide image-carousel style2 flexslider animated fadeInUp" data-animation="slide" data-item-width="270" data-item-margin="30" data-animation-type="fadeInUp" style="animation-duration: 1s; visibility: visible;">
                        
                    <div class="flex-viewport" style="overflow: hidden; position: relative;"><ul class="slides image-box" style="width: 1000%; transition-duration: 0s; transform: translate3d(-300px, 0px, 0px);">
                            <li style="width: 270px; float: left; display: block;">
                                <article class="box">
                                    <figure>
                                        <img src="images/Travel-Beans-Membership-50%-Off-at-Hotel-Stays.jpg" alt="" draggable="false">
                                    </figure>
                                    <div class="details">
                                        <h4>Enjoy Upto 50% OFF on Stays</h4>
                                        <p>Enjoy Upto 50% OFF On Your Holidays at All of Our Associated Hotels , Resorts and Other Accomodation Places .Feel the Luxury at Our 
										Hotels & Resorts .</p>
                                    </div>
                                </article>
                            </li>
                            <li style="width: 270px; float: left; display: block;">
                                <article class="box">
                                    <figure>
                                        <img src="images/Travel-Beans-Membership-30%-Off-on-Food-and-Other-Services.jpg" alt="" draggable="false">
                                    </figure>
                                    <div class="details">
                                        <h4>Flat 20-30% Discount on All Services in Hotel</h4>
                                        <p>All off Our Associated Properties Offers Discounts on All the Services , Also the Hotel Restaurants Offers
										Upto 30% Discount of Food Bills .</p>
                                    </div>
                                </article>
                            </li>
                            <li style="width: 270px; float: left; display: block;">
                                <article class="box">
                                    <figure>
                                        <img src="images/Travel-Beans-Membership-Free-Airport-Pick-and-Drop.jpg" alt="" draggable="false">
                                    </figure>
                                    <div class="details">
                                        <h4>Free Airport Pick & Drop</h4>
                                        <p>The Complimentary Nights Provide Free Pickups and Drops By Hotel Cabs For All the Guest Staying at the Hotel .This Offer is Only Valid
										Outside the Home City of Member .</p>
                                    </div>
                                </article>
                            </li>
							<li style="width: 270px; float: left; display: block;">
                                <article class="box">
                                    <figure>
                                        <img src="images/Travel-Beans-Membership-Complimentary-Breakfast.jpg" alt="" draggable="false">
                                    </figure>
                                    <div class="details">
                                        <h4>Complimentary Breakfast in Most of the Properties</h4>
                                        <p>The Signature Club Offers Complimentary Breakfast Buffet in Most of the Properties Associated With the Club . </p>
                                    </div>
                                </article>
                            </li>
							 <li style="width: 270px; float: left; display: block;">
                                <article class="box">
                                    <figure>
                                        <img src="images/Travel-Beans-Membership-Night-Complimentary.jpg" alt="" draggable="false">
                                    </figure>
                                    <div class="details">
                                        <h4>Flexible Combination of Nights</h4>
                                        <p>Use Your Package Nights in Whatever Way You Want . Out of All the Night Every Possible Breakups Can Be Scheduled to Book the Stays.
										 Maximum 3 Nights at Once .</p>
                                    </div>
                                </article>
                            </li>
							<li style="width: 270px; float: left; display: block;">
                                <article class="box">
                                    <figure>
                                        <img src="images/Travel-Beans-Membership-Gift-Vouchers.jpg" alt="" draggable="false">
                                    </figure>
                                    <div class="details">
                                        <h4>Gift Nights to Relatives </h4>
                                        <p>Gift Your Night to Your Family Relatives , Friends and Others . Let Them Enjoy Your Hospitality on Our Behalf , They Will Be Treated as Our
										Own Guests at Our Properties </p>
                                    </div>
                                </article>
                            </li>
							<li style="width: 270px; float: left; display: block;">
                                <article class="box">
                                    <figure>
                                        <img src="images/Travel-Beans-Membership-More-Pax-Adjusted.jpg" alt="" draggable="false">
                                    </figure>
                                    <div class="details">
                                        <h4>Accomodation of More PAX at Lowest</h4>
                                        <p>Complimentary Addition of 1 Adult in Every Room Under the Membership , Accompany Your Guests Or Relatives at Discounted Prices . </p>
                                    </div>
                                </article>
                            </li>
							<li style="width: 270px; float: left; display: block;">
                                <article class="box">
                                    <figure>
                                        <img src="images/Travel-Beans-Membership-For-Business-Sell-Nights-in-Profits.jpg" alt="" draggable="false">
                                    </figure>
                                    <div class="details">
                                        <h4>Membership For Business </h4>
                                        <p>Our Membership Offers Unique Business Idea for Our Members Where they Can Sell their Room Nights at Extra Profits and Get GST Invoices For
										Their Affiliate Customers </p>
                                    </div>
                                </article>
                            </li>
							<li style="width: 270px; float: left; display: block;">
                                <article class="box">
                                    <figure>
                                        <img src="images/Travel-Beans-Membership-AMC.jpg" alt="" draggable="false">
                                    </figure>
                                    <div class="details">
                                        <h4>Free of AMC Charges </h4>
                                        <p>Our Membership Doesn't Charge Any AMC (Annual Maintenance Fees or Charges) Unlike Every Other DMC Companies . We Value Service at One Time
										Payment and Cost .</p>
                                    </div>
                                </article>
                            </li>
                        </ul></div>
						</div>
            </div>
            
           
            
        </section>
        
        <footer id="footer" class="style5">
            <div class="footer-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <h2>The Travel Square</h2>
                            <ul class="discover triangle hover row">
                                <li class="col-xs-6"><a href="http://www.thetravelsquare.in/about-Travel-Square.html">About Company</a></li>
                                <li class="col-xs-6"><a href="http://www.thetravelsquare.in/jobs.html">Jobs & Carreers</a></li>
								<li class="col-xs-6"><a href="http://www.thetravelsquare.in/about-Travel-Square.html">Corporate Offices</a></li>
                                <li class="col-xs-6"><a href="http://www.thetravelsquare.in/legal.html">Legal Information</a></li>
                                <li class="col-xs-6"><a href="http://www.thetravelsquare.in/press.html">Press Release</a></li>
                                <li class="col-xs-6"><a href="http://www.thetravelsquare.in/legal.html">T & C</a></li>
                                <li class="col-xs-6"><a href="http://www.thetravelsquare.in/customer-care.html">Customer Care</a></li>
                                <li class="col-xs-6"><a href="http://www.thetravelsquare.in/legal.html">Privacy Policies</a></li>
                                <li class="col-xs-6"><a href="http://www.thetravelsquare.in/partner-Care.html">Partner Help</a></li>
                                <li class="col-xs-6"><a href="http://www.thetravelsquare.in/faq.html">FAQ's</a></li>
                                <li class="col-xs-6"><a href="http://www.thetravelsquare.in/site-Map.html">Site Map</a></li>
                                <li class="col-xs-6"><a href="http://www.thetravelsquare.in/contact-Us.html">Contact Us</a></li>
                            </ul>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <h2>Our Products</h2>
                            <ul class="discover triangle hover row">
                                <li class="col-xs-"><a href="http://www.thetravelsquare.in/pro">Travel Square Business</a></li>
                                <li class="col-xs-"><a href="http://www.signatureclubmembership.com/">Signature Club</a></li>
                                <li class="col-xs-"><a href="http://www.cruisader.com/">Cruisader</a></li>
                                <li class="col-xs-"><a href="http://www.cruisingindia.in/">Cruising India</a></li>
                                <li class="col-xs-"><a href="http://www.happenings.ind.in/">Happenings</a></li>
                                <li class="col-xs-"><a href="http://www.shoppist.me/">Shoppist</a></li>
                                
                            </ul>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <img src="images/Signature-Club-Logo.jpg">
                            <br>
                            
                            <br>
                            
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <h2>About Us</h2>
                            <p>Signature Club Founded in 2018 , Aims to Provide Complete Luxury During the Journey of Our Members . The Club Offers More than 75+ Destinations With 200+ Hotels , 
	Worldwide Covering Most of the Travel Destinations in the World .</p>
                            <br>
                            <address class="contact-details">
                                <center><a href="" class="contact-email">signatureclubmembership@gmail.com</a></center>
                            </address>
                            <ul class="social-icons clearfix">
                                <li class="twitter"><a title="Join Us On Twitter" href="https://twitter.com/signaturemember" data-toggle="tooltip" data-original-title="twitter" target="blank_"><i class="soap-icon-twitter"></i></a></li>
                                <li class="facebook"><a title="Join Us On Facebook" href="https://www.facebook.com/signatureclubmembership/" target="blank_" data-toggle="tooltip" data-original-title="facebook"><i class="soap-icon-facebook"></i></a></li>
                                <li class="instagram"><a title="Join Us On Instagram" href="https://www.instagram.com/signatureclubmembership/" target="blank_" data-toggle="tooltip" data-original-title="facebook"><i class="soap-icon-instagram"></i></a></li>
                                <li class="linkedin"><a title="Join Us On Linked In" href="" target="blank_" data-toggle="tooltip" data-original-title="linkedin"><i class="soap-icon-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottom gray-area">
                <div class="container">
                    
                    <div class="pull-right">
                        <a id="back-to-top" href="" class="animated bounce" data-animation-type="bounce" style="animation-duration: 1s; visibility: visible;"><i class="soap-icon-longarrow-up circle"></i></a>
                    </div>
                    <div class="copyright pull-right">
                        <p>© 2019 Signature Club | The Travel Square</p>
                    </div>
                </div>
            </div>
        </footer>
    </div>


    <!-- Javascript -->
    <script type="text/javascript" src="./Hotel Collections_files/jquery-1.11.1.min.js.download"></script>
    <script type="text/javascript" src="./Hotel Collections_files/jquery.noconflict.js.download"></script>
    <script type="text/javascript" src="./Hotel Collections_files/modernizr.2.7.1.min.js.download"></script>
    <script type="text/javascript" src="./Hotel Collections_files/jquery-migrate-1.2.1.min.js.download"></script>
    <script type="text/javascript" src="./Hotel Collections_files/jquery.placeholder.js.download"></script>
    <script type="text/javascript" src="./Hotel Collections_files/jquery-ui.1.10.4.min.js.download"></script>
    
    <!-- Twitter Bootstrap -->
    <script type="text/javascript" src="./Hotel Collections_files/bootstrap.js.download"></script>

    <!-- Flex Slider -->
    <script type="text/javascript" src="./Hotel Collections_files/jquery.flexslider-min.js.download"></script>
    
    <!-- parallax -->
    <script type="text/javascript" src="./Hotel Collections_files/jquery.stellar.min.js.download"></script>
    
    <!-- waypoint -->
    <script type="text/javascript" src="./Hotel Collections_files/waypoints.min.js.download"></script>

    <!-- load page Javascript -->
    <script type="text/javascript" src="./Hotel Collections_files/theme-scripts.js.download"></script>
    <script type="text/javascript" src="./Hotel Collections_files/scripts.js.download"></script>

    <script type="text/javascript">
        tjq("#slideshow .flexslider").flexslider({
            animation: "fade",
            controlNav: false,
            animationLoop: true,
            directionNav: false,
            slideshow: true,
            slideshowSpeed: 5000
        });
    </script>



