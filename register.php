<?php include('server.php') ?>

<!DOCTYPE html>

	<html>
	<head>
	
    <!-- Page Title -->
    <title>Signature Club Memberships | Travel Beans</title>
    
    <!-- Meta Tags -->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="author" content="Travel Beans">
    <meta name="description" content="">
    <meta name="distribution" content="global">
    <meta name="country" content="india">
    <meta name="copyright" content="Travel Beans">
    <meta name="revisit-after" content="1 hour">
    <meta name="designer" content="Travel Beans">
    <meta name="language" content="english">
    <meta name="robots" content="index, follow">
    <meta name="rating" content="general">
    <meta name="publisher" content="Travel Beans">

    <meta property="og:title" content="Signature Club | Travel Beans">
    <meta property="og:description" content="Signature Club Founded and Managed By Travel Beans (Good Vibes Travel Ventures Pvt. Ltd.) Group Was Established in 2018 .
	It Aims to Provide Luxury Properties at Affordable Prices to it's Members & Affiliates . The Club Offers More than 75+ Destinations With 200+ Hotels , 
	Worldwide Covering Most of the Travel Destinations in the World The Membership Offers to Standardise the Accomodation of It's Members to Have Leisure 
	Comfortable Feel While Travelling to Different Places or For Holidays .">
    <meta property="og:image" content="">
  
    <meta name="twitter:card" content="">
  
    <meta name="keywords" content="">
    <meta name="format-detection" content="telephone=no">
	
    
    <!-- Theme Styles -->
	<link rel="shortcut icon" href="" type="image/x-icon">
    <link rel="stylesheet" href="./Hotel Collections_files/bootstrap.min.css">
    <link rel="stylesheet" href="./Hotel Collections_files/font-awesome.min.css">
    <link rel="stylesheet" href="./Hotel Collections_files/animate.min.css">
	
    <link href="./Hotel Collections_files/css" rel="stylesheet" type="text/css">
    
    <!-- Main Style -->
	<style>
	.blink 
	{
	animation: blinker 1s linear infinite;
	}

	@keyframes blinker 
	{  
	75% { opacity: 0.0; }
	}
	</style>
	
    <script type="text/javascript">
        var mainStyle = "style.css";
        if (typeof localStorage != "undefined") {
            var colorSkin = localStorage.getItem("colorSkin");
            if (colorSkin != null) {
                mainStyle = "style-" + colorSkin + ".css";
            }
        }
        document.write('<link id="main-style" rel="stylesheet" href="css/' + mainStyle + '">');
    </script>
	
    <link id="main-style" rel="stylesheet" href="./Hotel Collections_files/style-light-yellow.css">
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <!-- Updated Styles -->
    <link rel="stylesheet" href="./Hotel Collections_files/updates.css">

    <!-- Custom Styles -->
    <link rel="stylesheet" href="./Hotel Collections_files/custom.css">
    
    <!-- Responsive Styles -->
    <link rel="stylesheet" href="./Hotel Collections_files/responsive.css">

	</head>

<body>
    <div id="page-wrapper">
        
		<header id="header" class="navbar-static-top">
            <div class="topnav hidden-xs">
                
            </div>
            
            <div class="main-header">
                
                <a href="#" data-toggle="collapse" class="mobile-menu-toggle">
                    Mobile Menu Toggle
                </a>

                <div class="container">
                    <h1 class="logo navbar-brand">
                        <a href="index.php" title="The Travel Sqaure">
                            <img src="logo.jpg" alt="The Travel Sqaure by Travel Beans">
                        </a>
                    </h1>
                    
                    <nav id="main-menu" role="navigation">
                        <ul class="menu">
                            <li class="menu-item-has-children">
                                <a href="http://www.signatureclubmembership.com/">Home</a>
                            </li>
							<li class="menu-item-has-children">
                                <a href="memberlist.php">All Members</a>
                            </li>
							<li class="menu-item-has-children">
                                <a href="allbookings.php">All Bookings</a>
                            </li>

                            
                            
                        </ul>
                    </nav>
                </div>
                
                <nav id="mobile-menu-01" class="mobile-menu collapse">
                    <ul id="mobile-primary-menu" class="menu">
                        <li class="menu-item-has-children">
                            <a href="http://www.signatureclubmembership.com/">Home</a><button class="dropdown-toggle collapsed" data-toggle="collapse" data-target="#mobile-menu-submenu-item-0"></button>
                            
                        </li>
                        <li class="menu-item-has-children">
                            <a href="">Hotels</a><button class="dropdown-toggle collapsed" data-toggle="collapse" data-target="#mobile-menu-submenu-item-2"></button>
                        </li>
                    </ul>
                </nav>	
            </div>
        </header>

		
		<section id="content">
            <div class="container">
			
        <div class="row">
                    <div id="main" class="col-sm-8 col-md-9">
                    <div class="travelo-box booking-section">
            <form class="booking-form" method="post" action="register.php">
                <div class="person-information">
                    <h2>Add New Member</h2>
  	                <?php include('errors.php'); ?>
  	        <div class="form-group row">
                  <div class="col-sm-6 col-md-5">
  	                <label>Member Name</label>
  	                    <input type="text" required class="input-text full-width" name="name"  placeholder="Member Name">
                 </div>
                 <div class="col-sm-6 col-md-5">
                    <label>Membership Types</label>
                        <div class="selector">
                            <select name="membership_type" class="full-width">
                                <option>Select</option>
                                <option>Premium</option>
                                <option>Prestige</option>
                                <option>Privilege</option>
                            </select>
                        </div>
                  </div>
            <div class="form-group row">
                  <div class="col-sm-6 col-md-5">
                     <label>Membership ID</label>
                        <input type="text" required class="input-text full-width" name="username" value="" placeholder="Membership ID">
                 </div>
                  <div class="col-sm-6 col-md-5">
  	                 <label>Password</label>
  	                    <input type="text" minlength="8" required class="input-text full-width" name="password_1" placeholder="Password">
                   </div>
            </div>
            <div class="form-group row">
                  <div class="col-sm-6 col-md-5">
                    <label>Total Nights</label>
                        <input type="number" required min="0" class="input-text full-width" name="tnight" placeholder="Total Nights">
                  </div>
                  <div class="col-sm-6 col-md-5">
                    <label>Nights Remaining</label>
                        <input type="number" required min="0" class="input-text full-width" name="rnight" placeholder="Night Remaining">
                  </div>
            </div>
            <div class="form-group row">
                  <div class="date picker-wrap col-sm-6 col-md-5">
                    <label>Date Of Birth</label>
                    <input type="date" name="dob" class="input-text full-width"> 
                    </div>
                  <div class="col-sm-6 col-md-5">
                    <label>Gender</label>
                        <div class="selector">
                            <select name="gender" class="full-width">
                                <option>Male</option>
                                <option>Female</option>
                                <option>Unspecified</option>
                            </select>
                        </div>
                  </div>
            </div>
            <div class="form-group row">                
                <div class="col-sm-6 col-md-10">
                    <label>Address</label>
                        <input type="address" required class="input-text full-width" name="address" placeholder="Address">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-6 col-md-5">
                    <label>E-mail Address</label>
                        <input type="email" required class="input-text full-width" name="email" placeholder="E-mail">
                </div>
                <div class="col-sm-6 col-md-5">
                    <label>Contact Number</label>
                        <input type="phone" required minlength="10" maxlength="10" class="input-text full-width" name="phone" placeholder="Contact Number">
                </div>
            </div>
          
            
            <button type="submit" class="button btn-medium sky-blue1 uppercase" name="reg_user">Register</button>
            </div>
                                    </div>




                                <hr>
                                
                            </form>
                        </div>
                    </div>
                    <div class="sidebar col-sm-4 col-md-3">
                        <div class="travelo-box contact-box">
                            <center><h4>Need Some Help ?</h4>
                            <p>Our Travel Sqaure Executives Are Always Ready Help You 24/7 .</p>
                            <address class="contact-details">
                                <span class="contact-phone">+91-9560001807</span><br>
                                <br>
                                <a class="contact-email" href="">partnerhelp@travelsqaure.com</a>
                            </address></center>
                        </div>
						<div class="travelo-box book-with-us-box">
                            <center><h4>Why Join Us ?</h4></center>
                            <ul>
                                <li>
                                    <i class="soap-icon-hotel-1 circle"></i>
                                    <h5 class="title"><a href="" class="skin-color">4,35,000+ Accomodations</a></h5>
                                    <p>We Gaurantee Best Supply Rates From Over 100+ Chains Of Hotels & Resorts .</p>
                                </li>
                                <li>
                                    <i class="soap-icon-savings circle"></i>
                                    <h5 class="title"><a href="#" class="skin-color">Lowest B2B Rates</a></h5>
                                    <p>We Gaurantee Lowest B2B Rates For You to Create More Margins & Profits .</p>
                                </li>
                                <li>
                                    <i class="soap-icon-support circle"></i>
                                    <h5 class="title"><a href="#" class="skin-color">24/7 Leads</a></h5>
                                    <p>We Gaurantee 24/7 Targeted Leads From the Travel Market For You .</p>
                                </li>
                            </ul>
                        </div>
						
                        
                    </div>
                </div>
            </div>
        </section>
        
        <footer id="footer" class="style5">
            <div class="footer-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <h2>The Travel Square</h2>
                            <ul class="discover triangle hover row">
                                <li class="col-xs-6"><a href="http://www.thetravelsquare.in/about-Travel-Square.html">About Company</a></li>
                                <li class="col-xs-6"><a href="http://www.thetravelsquare.in/jobs.html">Jobs & Carreers</a></li>
								<li class="col-xs-6"><a href="http://www.thetravelsquare.in/about-Travel-Square.html">Corporate Offices</a></li>
                                <li class="col-xs-6"><a href="http://www.thetravelsquare.in/legal.html">Legal Information</a></li>
                                <li class="col-xs-6"><a href="http://www.thetravelsquare.in/press.html">Press Release</a></li>
                                <li class="col-xs-6"><a href="http://www.thetravelsquare.in/legal.html">T & C</a></li>
                                <li class="col-xs-6"><a href="http://www.thetravelsquare.in/customer-care.html">Customer Care</a></li>
                                <li class="col-xs-6"><a href="http://www.thetravelsquare.in/legal.html">Privacy Policies</a></li>
                                <li class="col-xs-6"><a href="http://www.thetravelsquare.in/partner-Care.html">Partner Help</a></li>
                                <li class="col-xs-6"><a href="http://www.thetravelsquare.in/faq.html">FAQ's</a></li>
                                <li class="col-xs-6"><a href="http://www.thetravelsquare.in/site-Map.html">Site Map</a></li>
                                <li class="col-xs-6"><a href="http://www.thetravelsquare.in/contact-Us.html">Contact Us</a></li>
                            </ul>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <h2>Our Products</h2>
                            <ul class="discover triangle hover row">
                                <li class="col-xs-"><a href="http://www.thetravelsquare.in/pro">Travel Square Business</a></li>
                                <li class="col-xs-"><a href="http://www.signatureclubmembership.com/">Signature Club</a></li>
                                <li class="col-xs-"><a href="http://www.cruisader.com/">Cruisader</a></li>
                                <li class="col-xs-"><a href="http://www.cruisingindia.in/">Cruising India</a></li>
                                <li class="col-xs-"><a href="http://www.happenings.ind.in/">Happenings</a></li>
                                <li class="col-xs-"><a href="http://www.shoppist.me/">Shoppist</a></li>
                                
                            </ul>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <img src="images/Signature-Club-Logo.jpg">
                            <br>
                            
                            <br>
                            
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <h2>About Us</h2>
                            <p>Signature Club Founded in 2018 , Aims to Provide Complete Luxury During the Journey of Our Members . The Club Offers More than 75+ Destinations With 200+ Hotels , 
	Worldwide Covering Most of the Travel Destinations in the World .</p>
                            <br>
                            <address class="contact-details">
                                <center><a href="" class="contact-email">signatureclubmembership@gmail.com</a></center>
                            </address>
                            <ul class="social-icons clearfix">
                                <li class="twitter"><a title="Join Us On Twitter" href="https://twitter.com/signaturemember" data-toggle="tooltip" data-original-title="twitter" target="blank_"><i class="soap-icon-twitter"></i></a></li>
                                <li class="facebook"><a title="Join Us On Facebook" href="https://www.facebook.com/signatureclubmembership/" target="blank_" data-toggle="tooltip" data-original-title="facebook"><i class="soap-icon-facebook"></i></a></li>
                                <li class="instagram"><a title="Join Us On Instagram" href="https://www.instagram.com/signatureclubmembership/" target="blank_" data-toggle="tooltip" data-original-title="facebook"><i class="soap-icon-instagram"></i></a></li>
                                <li class="linkedin"><a title="Join Us On Linked In" href="" target="blank_" data-toggle="tooltip" data-original-title="linkedin"><i class="soap-icon-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottom gray-area">
                <div class="container">
                    
                    <div class="pull-right">
                        <a id="back-to-top" href="" class="animated bounce" data-animation-type="bounce" style="animation-duration: 1s; visibility: visible;"><i class="soap-icon-longarrow-up circle"></i></a>
                    </div>
                    <div class="copyright pull-right">
                        <p>© 2019 Signature Club | The Travel Square</p>
                    </div>
                </div>
            </div>
        </footer>
    </div>


    <!-- Javascript -->
    <script type="text/javascript" src="./Hotel Collections_files/jquery-1.11.1.min.js.download"></script>
    <script type="text/javascript" src="./Hotel Collections_files/jquery.noconflict.js.download"></script>
    <script type="text/javascript" src="./Hotel Collections_files/modernizr.2.7.1.min.js.download"></script>
    <script type="text/javascript" src="./Hotel Collections_files/jquery-migrate-1.2.1.min.js.download"></script>
    <script type="text/javascript" src="./Hotel Collections_files/jquery.placeholder.js.download"></script>
    <script type="text/javascript" src="./Hotel Collections_files/jquery-ui.1.10.4.min.js.download"></script>
    
    <!-- Twitter Bootstrap -->
    <script type="text/javascript" src="./Hotel Collections_files/bootstrap.js.download"></script>

    <!-- Flex Slider -->
    <script type="text/javascript" src="./Hotel Collections_files/jquery.flexslider-min.js.download"></script>
    
    <!-- parallax -->
    <script type="text/javascript" src="./Hotel Collections_files/jquery.stellar.min.js.download"></script>
    
    <!-- waypoint -->
    <script type="text/javascript" src="./Hotel Collections_files/waypoints.min.js.download"></script>

    <!-- load page Javascript -->
    <script type="text/javascript" src="./Hotel Collections_files/theme-scripts.js.download"></script>
    <script type="text/javascript" src="./Hotel Collections_files/scripts.js.download"></script>

    <script type="text/javascript">
        tjq("#slideshow .flexslider").flexslider({
            animation: "fade",
            controlNav: false,
            animationLoop: true,
            directionNav: false,
            slideshow: true,
            slideshowSpeed: 5000
        });
    </script>



